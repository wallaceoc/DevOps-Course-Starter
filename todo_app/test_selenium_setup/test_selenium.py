import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
 
# Module scope re-uses the fixture
@pytest.fixture(scope='module')
def driver():
    # path to your webdriver download
    # leave this empty if you want Selenium to download it for you
    opts = Options()
    opts.add_argument("--headless")
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-dev-shm-usage")
    with webdriver.Chrome(options=opts) as driver:
        yield driver
 
def test_python_home(driver):
    driver.get("https://www.python.org")
    assert driver.title == 'Welcome to Python.org'