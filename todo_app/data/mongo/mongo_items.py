from flask import session
from todo_app.data.item import Item
import os, pymongo

from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient
from bson.objectid import ObjectId

def get_secrets():
    runEnvironment = str(os.getenv('ENVIRONMENT'))
    if(runEnvironment == "test") or (runEnvironment == "local"):        
        mongodb_connection_str = os.getenv('MONGODB_CONNECTION_STRING')
        mongodb_name = os.getenv('MONGODB_NAME')
        todo_collection = os.getenv('MONGO_COLLECTION')
    else:
        keyVaultName = str(os.getenv('KEY_VAULT_NAME'))
        keyVaultUrl = f"https://{keyVaultName}.vault.azure.net"
        azureCredential = DefaultAzureCredential()
        client = SecretClient(vault_url=keyVaultUrl, credential=azureCredential)

        mongodb_connection_str = client.get_secret('MONGODB-CONNECTION-STRING').value
        mongodb_name = client.get_secret('MONGODB-NAME').value
        todo_collection = client.get_secret('MONGO-COLLECTION').value

    return {'mongodb_connection_str':mongodb_connection_str, 'mongodb_name':mongodb_name, 'todo_collection':todo_collection}

def get_mongodb(secrets):    
    mongodb_connection_str=secrets.get('mongodb_connection_str')
    mongodb_name=secrets.get('mongodb_name')    

    client = pymongo.MongoClient(mongodb_connection_str)
    return client[mongodb_name]

def get_mongo_collection():
    secrets = get_secrets()    
    db = get_mongodb(secrets)
    todo_collection = secrets.get('todo_collection')
    return db[todo_collection]

def get_items():
    """
    Fetches all saved items from the session.

    Returns:
        list: The list of saved items.
    """
    collection = get_mongo_collection()
    
    return build_card_items(collection)

def build_card_items(mongo_collection):
    card_items = []
    
    for mongo_item in mongo_collection.find():
        card_items.append(Item.from_mongo_record(mongo_item))

    return card_items

def get_item(id):
    """
    Fetches the saved item with the specified ID.

    Args:
        id: The ID of the item.

    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    object_id = ObjectId(id)
    collection = get_mongo_collection()
    return Item.from_mongo_record(collection.find_one({ "_id": object_id }))

def add_item(title, desc, status):
    """
    Adds a new item with the specified title to the session.

    Args:
        title: The title of the todo item.
        desc: The description of the todo item
        status: The status of the item

    Returns:
        item: The saved item.
    """
    # Add the item to the list
    collection = get_mongo_collection()
    record = collection.insert_one({"name": title, "desc": desc, "due": "", "status": status})

    return record.inserted_id

def save_item(id, status):
    """
    Updates an existing item in the session. If no existing item matches the ID of the specified item, nothing is saved.

    Args:
        id: The id of the item to update the status of.
        status: The new status of the  item.
    """
    object_id = ObjectId(id)
    collection = get_mongo_collection()
    
    filter = { "_id": object_id }
    newvalues = { "$set": { 'status': status } }

    record = collection.update_one(filter, newvalues)
    return id

