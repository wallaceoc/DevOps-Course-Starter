import os

TODO = "To Do"
DONE = "Done"

class Item:
    def __init__(self, id, name, desc, due_date, status = TODO):
        self.id = id
        self.name = name
        self.description = desc
        self.dueDate = due_date
        self.status = status        

    @classmethod
    def from_trello_card(cls, card):
        todo_list_id = os.getenv('CORNDEL_TODO_LIST_ID')
        trello_idList = card['idList']
        status = TODO if trello_idList == todo_list_id else DONE
        return cls(card['id'], card['name'], card['desc'], trello_idList, card['due'], status)
        
    def datePickerDueDate(self):
        if(self.dueDate is None):
            return ""
        return self.dueDate[0:10]
    
    @classmethod
    def from_mongo_record(cls, record):
        return cls(record['_id'], record['name'], record['desc'], record['due'], record['status'])