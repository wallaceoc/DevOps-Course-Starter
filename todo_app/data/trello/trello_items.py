from flask import session
from todo_app.data.item import Item
import os, requests

from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient

def get_secrets():
    runEnvironment = str(os.getenv('ENVIRONMENT'))
    if(runEnvironment == "test") or (runEnvironment == "local"):
        trelloApiKey = os.getenv('TRELLO_API_KEY')
        trelloToken = os.getenv('TRELLO_TOKEN')
        boardId = os.getenv('CORNDEL_BOARD_ID')
    else:
        keyVaultName = str(os.getenv('KEY_VAULT_NAME'))
        keyVaultUrl = f"https://{keyVaultName}.vault.azure.net"
        azureCredential = DefaultAzureCredential()
        client = SecretClient(vault_url=keyVaultUrl, credential=azureCredential)

        trelloApiKey = client.get_secret('TRELLOAPIKEY').value
        trelloToken = client.get_secret('TRELLOTOKEN').value
        boardId = client.get_secret('CORNDELBOARDID').value

    return {'trelloApiKey':trelloApiKey, 'trelloToken':trelloToken, 'boardId':boardId}

def get_items():
    """
    Fetches all saved items from the session.

    Returns:
        list: The list of saved items.
    """

    secrets = get_secrets()

    trelloApiKey=secrets.get('trelloApiKey')
    trelloToken=secrets.get('trelloToken')
    boardId = secrets.get('boardId')

    API = os.getenv('TRELLO_API')
    BOARD = os.getenv('TRELLO_BOARD').replace('{id}', boardId)
    
    # https://api.trello.com/1/board/{board_id}/cards
    request_url = API+BOARD+'?key='+trelloApiKey+'&token='+trelloToken
    trello_response = requests.get(request_url)

    return build_card_items(trello_response.json())

def build_card_items(trello_response):
    card_items = []
    
    for trello_list in trello_response:
        card_items.append(Item.from_trello_card(trello_list))

    return card_items

def get_item(id):
    """
    Fetches the saved item with the specified ID.

    Args:
        id: The ID of the item.

    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    items = get_items()
    return next((item for item in items if item.id == id), None)


def add_item(title, list_id):
    """
    Adds a new item with the specified title to the session.

    Args:
        title: The title of the card.
        list_id: The list to add the card to

    Returns:
        item: The saved item.
    """
    # Add the item to the list
    API = os.getenv('TRELLO_API')
    CARD = os.getenv('TRELLO_CARDS')

    secrets = get_secrets()

    trelloApiKey=secrets.get('trelloApiKey')
    trelloToken=secrets.get('trelloToken')

    request_url = API+CARD+'?key='+trelloApiKey+'&token='+trelloToken

    trello_response = requests.post(request_url, data={'name': title, 'idList': list_id})

    return trello_response

def save_item(card_id, list_id):
    """
    Updates an existing item in the session. If no existing item matches the ID of the specified item, nothing is saved.

    Args:
        card_id: The card_id to move to the new list.
        list_id: The list id to move the card to
    """

    API = os.getenv('TRELLO_API')
    CARD = os.getenv('TRELLO_CARD').replace('{id}', card_id)
    
    # https://api.trello.com/1/cards/{card_id}?key=....

    secrets = get_secrets()

    trelloApiKey=secrets.get('trelloApiKey')
    trelloToken=secrets.get('trelloToken')

    request_url = API+CARD+'?key='+trelloApiKey+'&token='+trelloToken

    trello_response = requests.put(request_url, data={'idList': list_id})

    return trello_response
