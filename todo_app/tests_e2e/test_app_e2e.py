import os
import pytest
from time import sleep
from threading import Thread
from todo_app import app
from todo_app.data import item
from todo_app.data.mongo.mongo_items import get_mongo_collection, add_item
from flask import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv, find_dotenv
import time

def delete_mongo_collection(collection):
    collection.drop()

def add_card_to_mongo_collection(title, desc, status):
    add_item(title, desc, status)

@pytest.fixture(scope="module")
def app_with_temp_mongo():
    # Use our test integration config instead of the 'real' version
    load_dotenv(override=True)

    os.environ['ENVIRONMENT'] = "test"

    collection = get_mongo_collection()
    add_card_to_mongo_collection('TestCard', '', item.TODO)

    # Construct the new application
    application = app.create_app()

    # Start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()
    # Give the app a moment to start
    sleep(1)

    yield application

    # Tear Down
    thread.join(1)
    delete_mongo_collection(collection)

@pytest.fixture(scope="module")
def driver():
    opts = Options()
    opts.add_argument("--headless")
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-dev-shm-usage")
    with webdriver.Chrome(options=opts) as driver:
        yield driver

def test_task_journey(driver, app_with_temp_mongo):
    driver.get('http://localhost:5000/')
 
    text_box = driver.find_element(By.ID, "todo-title")
    text_box.send_keys("NEW_TEST_CARD_Selenium4")
    add_button = driver.find_element(By.ID, "todo-submit")
    add_button.click()
    time.sleep(2)
    content = driver.page_source
    
    assert driver.title == 'To-Do App'
    assert 'NEW_TEST_CARD_Selenium4' in content
