import pytest
import mongomock
from todo_app.data.mongo.mongo_items import add_item, get_item
from todo_app.data import item
from todo_app import app
from dotenv import load_dotenv, find_dotenv

test_ids = []

@pytest.fixture(scope="session", autouse=True)
def client():
    # Use our test integration config instead of the 'real' version    
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    with mongomock.patch(servers=(('fakemongo.com', 27017),)):

        # Add the test data
        test_ids.append(add_item('TestCard1', 'Test desc', item.TODO))
        test_ids.append(add_item('TestCard2', 'Test desc 2', item.DONE))

        # Create the new app.
        test_app = app.create_app()
        # Use the app to create a test_client that can be used in our tests.
        with test_app.test_client() as client:
            yield client

def test_index_page(monkeypatch, client):
    # This replaces any call to requests.get with our own function
    response = client.get('/')

    assert response.status_code == 200
    assert 'TestCard1' in response.data.decode()
    assert 'Test desc 2' in response.data.decode()
    assert "text/html" in response.content_type

# Test update single item
def test_update_item(monkeypatch, client):
    before_status = get_item(test_ids[0]).status
    response = client.post('/todo/update_item', data={test_ids[0]: 'Update Status'})

    assert response.status_code == 302  # redirect
    assert False == response.is_json

    response = client.get('/')
    after_status = get_item(test_ids[0]).status

    assert response.status_code == 200
    assert 'TestCard1' in response.data.decode()
    assert 'Test desc 2' in response.data.decode()
    assert "text/html" in response.content_type
    assert before_status != after_status

# Test add item
def test_submit(monkeypatch, client):
    response = client.post('/todo/submit', data={'todo-title': 'TestAddCard2'})

    assert response.status_code == 302  # redirect
    assert False == response.is_json