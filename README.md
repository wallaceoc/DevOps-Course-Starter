# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

Module
TRELLO
Step 1
Create a new TRELLO account
Visit ['https://trello.com/signup'](https://trello.com/signup) in your browser and follow the setup instructions for a new account

Step 2
Create a new TRELLO API KEY and TOKEN
Visit ['https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/'](https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/) for detailed instructions on how to both create an API_KEY and TOKEN

## TESTING
Add pytest as a dependency of our project by running poetry add pytest. This should download pytest and also update pyproject.toml for you.
NOTE: This will only need to be run by one person and anyone following this should not need to as long as they run the "poetry install" command to update their dependencies.
This will get pulled in automatically for following users because it's now registered in the pyproject.toml
'''bash
$ poetry add pytest
'''

Run ALL the tests by typing pytest from the command line
'''bash
$ poetry run pytest
'''

Run the tests from a specific class
'''bash
poetry run pytest todo_app/tests/<file.py>
'''bash

## Selenium tests (end to end tests)
Install the selenium python package (this should be unnecessary as it is being tracked by Poetry but I had some issues and had to do this)
$ pip install -U selenium
$ poetry add selenium

Run the End to End tests
$ poetry run pytest todo_app/tests_e2e

## Ansible Provisioning
Run the below command to provision a VM from a Host node.
Note: you must ensure the necessary files from the playbook (in this case my-ansible-playbook.yml)
exist on the Controller

$ ansible-playbook my-ansible-playbook.yml -i ansible-inventory.ini

## Module 4 stretch goal

$ poetry add gunicorn

## Run in Docker

#### Development Mode

1. Build the development image
   '''bash
   docker build --target development --tag todo-app:dev .
   '''

2. Run the container (2 options)
   2.1 Docker Command
   '''bash
   docker run --env-file .env --publish 5000:80 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app/todo_app todo-app:dev
   '''

   2.2 Docker Compose.  This will start tests (unit, integration and end-to-end).  Tests will be automatically rerun when a file change is detected.  ***Note: If you add the --build option, it is forced to build the images even when not needed
   '''bash
   docker compose up [--build]
   '''

#### Production Mode
1. Build the production image
   '''bash
   docker build --target production --tag todo-app:prod .
   '''

2. Run the container
   '''bash
   docker run --env-file .env --publish 5000:80 todo-app:prod
   '''

### MODULE 5 + 7 Testing in Docler
#### Unit and Integration tests

# Build the docker image and run the tests
1. Build the test target
   '''bash
   docker build --target test --tag my-test-image .
   '''

2. Run the unit & integration tests
   '''bash
   docker run --env-file .env.test my-test-image tests
   '''

3. Run the end-to-end tests
   '''bash
   docker run --env-file .env my-test-image tests_e2e
   '''
or
   '''bash
   docker run -e SECRET_KEY -e CORNDEL_BOARD_ID -e CORNDEL_TODO_LIST_ID -e CORNDEL_DONE_LIST_ID -e TRELLO_API -e TRELLO_API_KEY -e TRELLO_CARDS -e TRELLO_TOKEN -e TRELLO_BOARD my-test-image tests_e2e
   '''

#### Alternatively
All tests can be run in Docker through docker-compose.

Unit & Integration tests:
'''bash
docker compose up --build todo_app_unit_test_runner
'''

End-to-end tests:
'''bash
docker compose up --build todo_app_e2e_test_runner
'''

4. Stretch goal for Test Coverage (this should only need to be run once but including for visibility)
$ pip install pytest-cov
$ poetry add pytest-cov
###

### MODULE 8 Exercise
1. Add the container image to Docker Hub registry
   # Part 1 Login to DockerHub
   ''' bash
   docker login
   '''

   # Part 2 Build the image
   ''' bash
   docker build --target <my_build_phase> --tag <image_tag> .
   '''
   #### Example: docker build --target production --tag wallaceoc/boc_todo_app:v0.0 .

   # Part 3 Push the image
   ''' bash
   docker push <image_tag>
   '''
   #### Example: docker push wallaceoc/boc_todo_app:v0.0

where <image_tag> has the format <user_name>/<image_name>:<tag>.

## Check out sample ToDo App
   https://woc180.azurewebsites.net/

## Add KeyVault for storing secrets
Install the Key Vault secrets library:
$ poetry add azure-identity
$ poetry add azure-keyvault-secrets
$ poetry add azure-keyvault

## Create a resource group and key vault
1. Use the az group create command to create a resource group:
 $ az group create --name <ResourceGroupName> --location eastus

2. Use az keyvault create to create the key vault:
 $ az keyvault create --name woc180-keyvault --resource-group <ResourceGroupName>
 e.g. ResourceGroupName = Cohort29_BilOCo_ProjectExercise

3. Grant access to your key vault
 $ az keyvault set-policy --name woc180-keyvault --upn Bil.OCo@devops.corndel.com --secret-permissions delete get list set

 ## Enrcyption at Rest

 From the Microsoft Docs for Cosmos DB:
 #
 "Encryption at rest" is a phrase that commonly refers to the encryption of data on nonvolatile storage devices, such as solid-state drives (SSDs) and hard-disk drives (HDDs). Azure Cosmos DB stores its primary databases on SSDs. Its media attachments and backups are stored in Azure Blob Storage, which are generally backed up by HDDs. With the release of encryption at rest for Azure Cosmos DB, all your databases, media attachments, and backups are encrypted. Your data is now encrypted in transit (over the network) and at rest (nonvolatile storage), giving you end-to-end encryption. 
 #

 In summary, "Encryption at rest" for Cosmos DB on Azure is enabled (or "on") by default and there is no way to turn it off.  Azure Cosmos DB uses AES-256 encryption an is implemented using several security technologies, such as secure key systems, encrypted networks, and cryptographic APIs.