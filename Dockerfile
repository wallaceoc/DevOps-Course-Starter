FROM python:3.11.4-buster as base

# Perform common operations, dependency installations etc...

RUN apt-get update 

ENV PORT=80
EXPOSE $PORT
ENV PATH="/root/.local/bin:$PATH"

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /app/todo_app
COPY pyproject.toml poetry.lock /app/todo_app/

# Configure for production
FROM base as production

RUN poetry install --without dev
COPY todo_app todo_app

ENTRYPOINT poetry run gunicorn --bind 0.0.0.0:$PORT "todo_app.app:create_app()"

##
# Configure for development
FROM base as development

RUN poetry install
COPY todo_app todo_app

ENTRYPOINT [ "/bin/bash", "-c", "poetry run flask run --host=0.0.0.0 --port=$PORT" ]
##

# Configure for testing stage
FROM base as test

RUN curl -sSL https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o chrome.deb &&\     
    apt-get update && apt-get install ./chrome.deb -y &&\  
    rm ./chrome.deb 

RUN apt-get install -yqq unzip jq
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/bin/chromedriver

RUN poetry install
COPY todo_app todo_app
COPY .env.test .
COPY todo_app/tests tests
COPY todo_app/tests_e2e tests_e2e

ENTRYPOINT [ "poetry", "run", "pytest"] 